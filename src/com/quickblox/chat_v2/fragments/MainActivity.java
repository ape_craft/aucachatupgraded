package com.quickblox.chat_v2.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.quickblox.chat_v2.R;
import com.quickblox.chat_v2.core.ChatApplication;
import com.quickblox.chat_v2.gcm.GCMHelper;
import com.quickblox.chat_v2.interfaces.OnUserProfileDownloaded;
import com.quickblox.chat_v2.utils.ContextForDownloadUser;
import com.quickblox.module.users.model.QBUser;

public class MainActivity extends SherlockFragmentActivity implements OnUserProfileDownloaded{

	// Declare Variables
	ActionBar mActionBar;
	ViewPager mPager;
	Tab tab;

	private ChatApplication app;
	private ProgressDialog progress;

	private BroadcastReceiver mDialogsRefresh = new BroadcastReceiver() {
		@Override
		public void onReceive(Context pContext, Intent pIntent) {

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Get the view from activity_main.xml
		setContentView(R.layout.activity_main);

		app = ChatApplication.getInstance();
		GCMHelper.register(this);
		
		setupTabs();
		
		switchProgressDialog(true);
        if(app.getRstManager() == null){
             Log.e("ERROR", "ROSTER manager=nil");
        }else{
            app.getRstManager().sendPresence(this);
        }
        
        downloadStartUpInfo();
	}
	
	public void switchProgressDialog(boolean enable) {
        if (enable) {
            progress = ProgressDialog.show(this, getResources().getString(R.string.app_name), getResources().getString(R.string.loading), true);
        } else {

            if (progress == null) {
                return;
            }
            progress.dismiss();
        }
    }

	private void setupTabs() {
		// Activate Navigation Mode Tabs
		mActionBar = getSupportActionBar();

		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Locate ViewPager in activity_main.xml
		mPager = (ViewPager) findViewById(R.id.pager);

		// Activate Fragment Manager
		FragmentManager fm = getSupportFragmentManager();

		// Capture ViewPager page swipes
		ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				super.onPageSelected(position);
				// Find the ViewPager Position
				mActionBar.setSelectedNavigationItem(position);
			}
		};

		mPager.setOnPageChangeListener(ViewPagerListener);
		// Locate the adapter class called ViewPagerAdapter.java
		ViewPagerAdapter viewpageradapter = new ViewPagerAdapter(fm);
		// Set the View Pager Adapter into ViewPager
		mPager.setAdapter(viewpageradapter);

		// Capture tab button clicks
		ActionBar.TabListener tabListener = new ActionBar.TabListener() {

			@Override
			public void onTabSelected(Tab tab, FragmentTransaction ft) {
				// Pass the position on tab click to ViewPager
				mPager.setCurrentItem(tab.getPosition());
			}

			@Override
			public void onTabUnselected(Tab tab, FragmentTransaction ft) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onTabReselected(Tab tab, FragmentTransaction ft) {
				// TODO Auto-generated method stub
			}
		};

		// Create first Tab
		tab = mActionBar.newTab().setText("Dilaogs")
				.setTabListener(tabListener);
		mActionBar.addTab(tab);

		// Create second Tab
		tab = mActionBar.newTab().setText("Chat Rooms")
				.setTabListener(tabListener);
		mActionBar.addTab(tab);

		// Create third Tab
		tab = mActionBar.newTab().setText("Contacts")
				.setTabListener(tabListener);
		mActionBar.addTab(tab);

		tab = mActionBar.newTab().setText("Profile")
				.setTabListener(tabListener);
		mActionBar.addTab(tab);
	}
	
	private void downloadStartUpInfo() {
        app.getQbm().addUserProfileListener(this);
        app.getMsgManager().downloadPersistentRoom();
        boolean isNeedLoadUsersFromQb = app.getRstManager().getContactListFromRoster();
        if (!isNeedLoadUsersFromQb) {
            switchProgressDialog(false);
            app.getQbm().removeUserProfileListener(this);
        }

    }

	@Override
	public void downloadComplete(QBUser friend, ContextForDownloadUser pContextForDownloadUser) {
        if (pContextForDownloadUser == ContextForDownloadUser.DOWNLOAD_FOR_MAIN_ACTIVITY) {
            switchProgressDialog(false);
            app.getQbm().removeUserProfileListener(MainActivity.this);
        }
    }
}
