package com.quickblox.chat_v2.interfaces;

public interface OnRoomListDownloaded {
	
	public void roomListDownloaded();
}
