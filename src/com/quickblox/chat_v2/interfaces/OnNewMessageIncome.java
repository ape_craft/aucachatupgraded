package com.quickblox.chat_v2.interfaces;

public interface OnNewMessageIncome {
	
	public void incomeNewMessage(String messageBody);
}
