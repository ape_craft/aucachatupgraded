package com.quickblox.chat_v2.interfaces;

public interface OnDialogCreateComplete {

	public void dialogCreate(int userId, String customObjectUid);
	
}
