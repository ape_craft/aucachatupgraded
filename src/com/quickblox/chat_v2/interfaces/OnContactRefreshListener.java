package com.quickblox.chat_v2.interfaces;

public interface OnContactRefreshListener {
    public void onRefreshCurrentList();
}
