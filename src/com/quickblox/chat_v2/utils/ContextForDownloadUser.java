package com.quickblox.chat_v2.utils;

/**
 * Created by andrey on 03.07.13.
 */
public enum ContextForDownloadUser {

    DOWNLOAD_FOR_DIALOG,
    DOWNLOAD_FOR_CONTACTS,
    DOWNLOAD_FOR_MESSAGE_MANAGER,
    DOWNLOAD_FOR_MAIN_ACTIVITY,
    DOWNLOAD_FOR_FRIENDS_ACTIVITY,
    DOWNLOAD_FOR_ROSTER
}
